//
//  LinksViewController.m
//  ReferenceMarketing
//
//  Created by Dirac on 4/21/15.
//  Copyright (c) 2015 getambassador. All rights reserved.
//

#import "LinksViewController.h"
#import "LinksTableViewCell.h"
#import "SpecificLinkViewController.h"


@interface LinksViewController () < UIAlertViewDelegate>

@end

@implementation LinksViewController

@synthesize tblViewLinks;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    linksDictionary = [NSMutableDictionary dictionary];
    selectedRow = 0;
    
     self.navigationController.navigationBarHidden = YES;
    


}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
    NSDictionary* existingEntries = [[NSUserDefaults standardUserDefaults] objectForKey:localdbkey];
    
    if (existingEntries != nil) {
        
        linksDictionary = [[NSMutableDictionary alloc] initWithDictionary:existingEntries];
    }

    
    [tblViewLinks reloadData];
}



// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    SpecificLinkViewController* specific =  (SpecificLinkViewController*) [segue destinationViewController];
    
    specific.linkName = [[linksDictionary allKeys] objectAtIndex:selectedRow];

}




- (IBAction)axnAddLink:(id)sender {
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Add Link" message:@"Please enter a reference link." delegate:self cancelButtonTitle:@"Submit" otherButtonTitles:@"Skip", nil];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    [alertView show];
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {
        return;
    }
    
    UITextField *linkNameTextField = [alertView textFieldAtIndex:0];
   
    bool editingWorkflow = false;
    if (alertView.tag == 1000) {
        //editing workflow
        
        editingWorkflow = true;
    
    }
    
    //check if it already exists - if it does then don't allow it to be added
    
    NSString* linkName = linkNameTextField.text;
    
    if ([linksDictionary objectForKey:linkName] == nil) {
        
        int countToBeUsed = 0;
        
        if (editingWorkflow) {
            
            countToBeUsed = [[linksDictionary objectForKey:linkNameToBeEdited] intValue];
            
            [linksDictionary removeObjectForKey:linkNameToBeEdited];
            
        }
        
        
        [linksDictionary setObject:@(countToBeUsed) forKey:linkName];
        
        
        [[NSUserDefaults standardUserDefaults] setObject:linksDictionary forKey:localdbkey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [tblViewLinks reloadData];
    }
    else
    {
        
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Duplicate Link" message:@"Please enter another reference link." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertView show];
    }
    
}




#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [linksDictionary count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    LinksTableViewCell* cell = [tblViewLinks dequeueReusableCellWithIdentifier:@"LinkTableCell"];
    
    NSArray* keys = [linksDictionary allKeys];
    
    NSString* linkName = [keys objectAtIndex:indexPath.row];
    
    int clickCount = [[linksDictionary objectForKey:linkName] intValue];
    
    cell.path = indexPath;
    cell.editDelegate = self;
    [cell.lblLinkName setText:linkName];
    
    [cell.lblClickCount setText:[NSString stringWithFormat:@"%d", clickCount]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* linkToBeDeleted = [[linksDictionary allKeys] objectAtIndex:indexPath.row];
    
    [linksDictionary removeObjectForKey:linkToBeDeleted];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:linksDictionary forKey:localdbkey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [tblViewLinks reloadData];
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    selectedRow = indexPath.row;
    [self performSegueWithIdentifier:@"linktapped" sender:self];
}


#pragma mark - EditButtonDelegate

-(void) editButtonClickedAt:(NSIndexPath*) path forLinkName:(NSString*) linkName{
    
    linkNameToBeEdited = linkName;
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Modify Link name ?" message:@"Please enter a new reference link." delegate:self cancelButtonTitle:@"Submit" otherButtonTitles:@"Skip", nil];
    
    alertView.tag = 1000;
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    [alertView show];
}



@end
