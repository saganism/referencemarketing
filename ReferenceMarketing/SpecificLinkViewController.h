//
//  SpecificLinkViewController.h
//  ReferenceMarketing
//
//  Created by Dirac on 4/21/15.
//  Copyright (c) 2015 getambassador. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LinksViewController.h"
@interface SpecificLinkViewController : UIViewController

@property (nonatomic, strong) NSString* linkName;
@property (strong, nonatomic) IBOutlet UILabel *lblLinkName;

@end
