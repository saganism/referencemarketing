//
//  LinksTableViewCell.m
//  ReferenceMarketing
//
//  Created by Dirac on 4/21/15.
//  Copyright (c) 2015 getambassador. All rights reserved.
//

#import "LinksTableViewCell.h"

@implementation LinksTableViewCell

@synthesize lblClickCount, lblLinkName, btnEdit;

@synthesize editDelegate, path;


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



- (IBAction)axnEdit:(id)sender {
    
    [editDelegate editButtonClickedAt:path forLinkName:lblLinkName.text];
}

@end
