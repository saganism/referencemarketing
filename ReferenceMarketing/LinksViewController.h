//
//  LinksViewController.h
//  ReferenceMarketing
//
//  Created by Dirac on 4/21/15.
//  Copyright (c) 2015 getambassador. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditButtonDelegate.h"

#define localdbkey @"localdb"


@interface LinksViewController : UIViewController<EditButtonDelegate>{
    
    NSMutableDictionary* linksDictionary;
    int selectedRow;
    
    NSString* linkNameToBeEdited;

}

@property (strong, nonatomic) IBOutlet UITableView *tblViewLinks;
@end
