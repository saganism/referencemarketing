//
//  LinksTableViewCell.h
//  ReferenceMarketing
//
//  Created by Dirac on 4/21/15.
//  Copyright (c) 2015 getambassador. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditButtonDelegate.h"

@interface LinksTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblLinkName;
@property (strong, nonatomic) IBOutlet UILabel *lblClickCount;
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;

@property (strong, nonatomic) id<EditButtonDelegate> editDelegate;
@property (strong, nonatomic) NSIndexPath* path;

@end
