//
//  SpecificLinkViewController.m
//  ReferenceMarketing
//
//  Created by Dirac on 4/21/15.
//  Copyright (c) 2015 getambassador. All rights reserved.
//

#import "SpecificLinkViewController.h"

@interface SpecificLinkViewController ()

@end

@implementation SpecificLinkViewController

@synthesize linkName, lblLinkName;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBarHidden = YES;
    
    lblLinkName.text = linkName;
    
    [self updateModelObject];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)axnGoBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}



-(void) updateModelObject{
    
    
    NSDictionary* storedDictionary = [[NSUserDefaults standardUserDefaults] objectForKey:localdbkey];
    
    
    NSMutableDictionary* mutableDictionary = [[NSMutableDictionary alloc] initWithDictionary:storedDictionary];
    
    
    int clickCount = [[storedDictionary objectForKey:linkName] intValue];
    
    clickCount++;
    
    [mutableDictionary setObject:@(clickCount) forKey:linkName];
    
    [[NSUserDefaults standardUserDefaults] setObject:mutableDictionary forKey:localdbkey];
    
    [[NSUserDefaults standardUserDefaults] synchronize];

}
@end
